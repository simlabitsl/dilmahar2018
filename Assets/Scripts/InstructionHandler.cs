﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionHandler : MonoBehaviour {


    public GameObject[] Steps;
    int count;
    GameObject tempObj;
    // Use this for initialization
    void Start ()
    {
       // Steps = new GameObject[3];
        count = 0;
	}
	
	// Update is called once per frame
	public void NextClick ()
    {
        DisableAll();
        if (count>=Steps.Length)
        {
            count = 0;
        }
        Steps[count].SetActive(true);
        count++;
    }
    public void DisableAll()
    {
        Steps[0].SetActive(false);
        Steps[1].SetActive(false);
        Steps[2].SetActive(false);
    }
}
